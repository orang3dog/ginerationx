# Welcome to GINeration X Distillary R&D Labs

Less code, more mixology, same lifecycle management

# History

* Holland
* 1700s
* Medicine for stomach issues and stuff
* "Dutch Courage" as relief to soliders fighting
* Brits soliders took back home and starting making small batches
* 2 criterias make Gin, Gin
  * 1) neutral spirt (ethanol), the base
  * 2) juniper berries
* everything else is the flavor profile which makes them all unique

# Masters of it

* Hendrick's uses Coriander, Lemon Peel, Grains of Paridise, Angelica Root, Cubeb Berries, Cuccumber, Rose
* Tanqueray won't say beyond Coriander and Angelica Root
* Plymouth Gin less juniper and more sweet citrus

# Flavor Guide

![Flavor Wheel](gin.jpg "Flavor Wheel")

# Kudos

Thanks to the DIY Kit for Real Gin Making Kit to inspire the fun